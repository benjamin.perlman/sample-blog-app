from ast import keyword
from curses import keyname
from django.contrib import admin

from posts.models import Post, Comment, Keyword

# Register your models here.
class PostAdmin(admin.ModelAdmin):
    pass


class CommentAdmin(admin.ModelAdmin):
    pass


class KeywordAdmin(admin.ModelAdmin):
    pass


admin.site.register(Post, PostAdmin)

admin.site.register(Comment, CommentAdmin)

admin.site.register(Keyword, KeywordAdmin)
